# Supply

## Overview
<img src="docs/supply_fm.png"  width="538" height="300">

## Description

The adapter module allows to power the entire demonstrator by connecting an external power supply to an available branch port of any node. The module protects the electrical layer from voltage spikes using a transient-voltage-suppression (TVS) diode, but attention should be paid when connecting the supply to the adapter, because the module **does NOT protect against reverse polarity**.

## Technical Details

|                   |                           |
| ----              | ----                      |
| Dimensions        | 25 x 31 x 10 [mm] (WxLxH) |
| Max current       | 13.5 [A]                  |

## Media

[<img src="docs/supply_1.png"  width="137" height="100">](docs/supply_1.png)
&nbsp;
[<img src="docs/supply_2.png"  width="143" height="100">](docs/supply_2.png)

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/grid/supply/supply.md)
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/grid/supply/supply.md)
