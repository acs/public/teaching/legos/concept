# Node

## Overview
<img src="docs/node_fm.png"  width="415" height="300">

## Description

The node is a passive device for power distribution that allows the interconnection of up to six branches on the edge and an entity on top. It allows the design of compact triangular grid meshes with self-mapping capabilities: each port can be identified by a 64-bit unique ID accessible via 1-wire. Branches are connected via ultra-low profile pin headers, whereas entities via a custom designed spring-loaded connector.

## Technical Details

|                   |                           |
| ----              | ----                      |
| Dimensions        | 60 x 60 x 7 [mm] (WxLxH)  |
| Max bus current   | 8 [A]                     |

## Media

[<img src="docs/node1.png"  width="113" height="100">](docs/node1.png)
&nbsp;
[<img src="docs/node2.png"  width="95" height="100">](docs/node2.png)

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/grid/node/node.md)
* [ID-Table]()
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/grid/node/node.md)
