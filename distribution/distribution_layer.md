# Distribution layer

<img src="docs/grid_3n.png"  width="905" height="300">

## Description
The electrical distribution layer consists of a real network beneath the visible model, made of branches and nodes that allow the power to flow according to a defined [topology](topology/topology.md). On top of this layer, the entities in the [application layer](../application/application_layer.md) interact among them by exchanging power according to their behavior. The building blocks are nodes and branches, connected in a triangular mesh network that allows a maximum of six branches to be connected to the same node. The [minimum allowed network](docs/grid_2n.png) is given by one branch and two nodes, whereas branches not properly terminated on a node will not be able to work properly.

## Features
* single supply +5V / 60W with multiple entries allowed
* power layer (5V) for off-grid emulation (e.g. power generation source, sensors, led power flow..)
* distribution layer (3.3V) for metered on-grid operations (e.g. power generation/consumption, power flow measurements, grid reconfiguration..)
* on-connection topology self-mapping technology
* physics-driven power flow visualization: direction, magnitude
* independent IoT devices with common monitor and control capabilities

## Elements
* [Node](node/node.md): grid building block; interconnects multiple branches in a triangular mesh network
* [Branch](branch/branch.md): grid building block; interconnects two nodes monitoring and controlling the resulting power flow
* [Supply](supply/supply.md): grid building block; provides power supply to the entire demonstrator
