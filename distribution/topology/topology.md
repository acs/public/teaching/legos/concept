# Topology
## Description
The selected topology includes traditional and renewable power generation sources, power critical structures, power hungry entities and residential prosumers. Alternative topologies to the one proposed can also be adopted. In case of short-circuit, the fault is first located and isolated; then, in case of fault on non-terminal feeders, the FLISR service can reconfigure automatically the network to restore the service in the affected portion of the grid.

<img src="docs/topology.png"  width="960" height="460">

## Features
* empty nodes provide network interconnection
* filled nodes accomodate entities on top
* dashed lines are in normally open state, reconfigurable on fault
* solid lines are in normally closed state, reconfigurable on fault