# Solar Farm

## Overview
<img src="docs/solar_farm_fm.png"  width="453" height="300">

[<img src="docs/solar_farm_3d_1.png"  width="117" height="100">](docs/solar_farm_3d_1.png)


### Descripton
The solar farm is one of the power producers running on renewable energy, in the specific by converting the solar energy into electricity. The higher is the intensity of the solar energy, the higher is the power produced and injected into the grid. The current power production is shown by a LED indicator, where the maximum represents a contribution of 20% to the total production. In order to optimize the exposure to sunlight, a 1D solar tracker tilts the PV cells following the maximum light intensity.

### Interaction
<img src="docs/solar_farm_bd.png"  width="490" height= "275">

### Features
<img src="docs/solar_farm_ft_1.png"  width="424" height= "300">
<img src="docs/solar_farm_ft_2.png"  width="294" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 116 x 116 x 49 [mm] (WxLxH)   |
| Operating voltage | 5 [V]                         |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 75 [mΩ]                       |
| Start delay       | 50 [ms]                       |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LT3081    | 1         |         | 1000 mA |
| ESP32     | 1         | 160 mA  |  260 mA |
| Servo 	| 2 		|         |  250 mA |
| LED Gauge | 1(5) 		| 10 mA   |  100 mA	|
| Total     |           |         | 1610 mA |


## Media

[<img src="docs/solar_farm_1.png" width="106" height= "100">](docs/solar_farm_1.png)
&nbsp;
[<img src="docs/solar_farm_2.png" width="117" height= "100">](docs/solar_farm_2.png)

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/solar_farm/solar_farm.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/solar_farm/solar_farm.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/solar_farm/solar_farm.md)