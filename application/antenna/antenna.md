# Antenna

## Overview
<img src="docs/antenna_fm.png"  width="420" height="300">

[<img src="docs/antenna_3d.png"  width="53" height="100">](docs/antenna_3d.png)

## Descripton
The antenna is the interface between demonstrator and cloud. Based on a Raspberry Pi single board cmputer, the antenna provides network connectivity via Wi-Fi to all the entities and at the same time hosts the edge cloud services. All the data exchanged between the antenna and the entities are based on the MQTT message protocol with UltraLight 2.0 encoding. The antenna also hosts a web server accessible via smartphone that provides redirection to each entity for direct web-based interaction.

### Interaction
The network activity of the antenna is visualized by LEDs on the side of the tower

<img src="docs/antenna_bd.png"  width="252" height= "100">

### Features
<img src="docs/antenna_ft.png"  width="220" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 120 x 120 x 257 [mm] (WxLxH)  |
| Operating voltage | 5 [V]                         |
| Monitored bus     | 5 [V]                         |
| Shunt resistance  | 50 [mΩ]                       |
| Start delay       | 11 [s]                        |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| RPi 3     | 1 		|  		  | 800 mA  |
| LED 	    | 12 		| 60 mA   | 120 mA	|
| Total     |           |         | 920 mA  |

## Media

[<img src="docs/antenna_1.png" width="46" height= "100">](docs/antenna_1.png)
&nbsp;
[<img src="docs/antenna_2.png" width="40" height= "100">](docs/antenna_2.png)

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/antenna/antenna.md)
* [Software]() 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/antenna/antenna.md)
