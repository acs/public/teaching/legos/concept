# Electric Car Station

## Overview
<img src="docs/ec_station_fm.png"  width="376" height="300">

[<img src="docs/ec_station_3d.png"  width="93" height="100">](docs/ec_station_3d.png)
&nbsp;

## Descripton
The electric-car station is an essential infrastructure for mobility based on electric vehicles, but can also be a significant load for the grid. The station can host up to two vehicles per time, each one absorbing power according to a custom profile stored inside an RFID tag. When the vehicle is placed in the charging slot, the RFID scanner detects the tag, read the contents and begins the charging procedure.

### Interaction
<img src="docs/ec_station_bd.png"  width="252" height= "200">

### Features
<img src="docs/ec_station_ft.png"  width="369" height= "300">

### Charging Patterns
The charging patterns are stored in hex format inside the RFID tag. When scanned, a current proportional to the stored values is absorbed from the grid, where 100% corresponds to a nominal current of 3.3 V / 30 Ω = 110 mA.

Car1(%):

        0x00, 0x64, 0x00, 0x64, //   0,   100,   0,  100,
        0x00, 0x64, 0x00, 0x64, //   0,   100,   0,  100,
        0x32, 0x00, 0x32, 0x64, //  50,     0,  50,  100,
        0x32, 0x00, 0x32, 0x00  //  50,     0,  50,    0

Car2(%):

        0x64, 0x32, 0x64, 0x32, //  100,    50,   100,   50,
        0x64, 0x48, 0x32, 0x00, //  100,    75,    50,    0,
        0x32, 0x48, 0x64, 0x64, //   50,    75,   100,  100,
        0x32, 0x64, 0x32, 0x00  //   50,   100,    50,    0

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 99 x 96 x 51 [mm] (WxLxH)     |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 50 [mΩ]                       |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  | 260 mA  |
| V Load    | 1         |         | 110 mA  |
| INA 233   | 1         |         |   5 mA  |
| Total     |           |         | 375 mA  |

### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LED Car   | 1(2)      | 10 mA   | 40 mA   |
| Total     |           |         | 40 mA   |

## Media

[<img src="docs/ec_station_1.png" width="174" height= "100">](docs/ec_station_1.png)
&nbsp;

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/ec_station/ec_station.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/ec_station/ec_station.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/ec_station/ec_station.md)