# Data Center

## Overview
<img src="docs/datacenter_fm.png"  width="376" height="300">

[<img src="docs/datacenter_3d_1.png"  width="93" height="100">](docs/datacenter_3d_1.png)
&nbsp;
[<img src="docs/datacenter_3d_2.png"  width="95" height="100">](docs/datacenter_3d_2.png)

## Descripton
The datacenter is a key infrastructure for IT operations, providing computation and data storage services. Being crucial for business continuity, even during a possible power grid failure event, the datacenter operation is supported by an internal uninterruptible power supply (UPS) that allows to continue operating during a fault. During normal operation, the UPS uses the energy from grid to load the internal battery storage, whereas during a fault the UPS provides the required power.

### Interaction
The hard-drive activity of the datacenter is visualized by LEDs in the servers. LEDs on the base show the flow of energy from the grid to the UPS or viceversa in case of fault.

<img src="docs/datacenter_bd.png"  width="504" height= "147">

### Features
<img src="docs/datacenter_ft.png"  width="343" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 108 x 99 x 70 [mm] (WxLxH)    |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 100 [mΩ]                      |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| LED Server| 6			|  30 mA  |   60 mA	|
| LED Status| 6			|  30 mA  |   60 mA	|
| SD-Reader | 1			|         |  500 mA	|
| UPS       | 1 		|  		  |  100 mA |
| INA 223	| 1			|		  |    5 mA	|
| Total     |           |         |  985 mA |


### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LED UPS   | 4 		| 40 mA   | 80 mA	|
| Total     |           |         | 80 mA   |

## Media

[<img src="docs/datacenter_1.png" width=" 98" height= "100">](docs/datacenter_1.png)
&nbsp;
[<img src="docs/datacenter_2.png" width="127" height= "100">](docs/datacenter_2.png)
&nbsp;
[<img src="docs/datacenter_3.png" width="105" height= "100">](docs/datacenter_3.png)

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/data_center/data_center.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/data_center/data_center.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/data_center/data_center.md)
