# Factory

## Overview
<img src="docs/factory_fm.png"  width="468" height="300">

[<img src="docs/factory_3d_1.png"  width="75" height="100">](docs/factory_3d_1.png)
&nbsp;
[<img src="docs/factory_3d_2.png"  width="84" height="100">](docs/factory_3d_2.png)

### Descripton
The factory is a typical industrial consumer with a significant load effect on the grid. The manufacturing process takes place in the form of an assembly line, including the movement of robotic arms and welding stages. The current electrical load is related to the factory workload, which can altered by using the increment/decrement touch buttons. 

### Interaction
<img src="docs/factory_bd.png"  width="252" height= "247">

### Features
<img src="docs/factory_ft_1.png"  width="405" height= "300">
<img src="docs/factory_ft_2.png"  width="500" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 112 x 108 x 148 [mm] (WxLxH)  |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 100 [mΩ]                      |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| LED Welder| 1(2)  	|  63 mA  |  250 mA	|
| INA 223	| 1			|		  |    5 mA	|
| Total     |           |         |  515 mA |

### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| Servo     | 1 		|         |  		|
| LED Gauge | 1(5) 		| 10 mA   | 100 mA	|
| Total     |           |         | 100 mA  |

## Media

[<img src="docs/factory_1.png" width="82" height= "100">](docs/factory_1.png)
&nbsp;
[<img src="docs/factory_2.png" width="64" height= "100">](docs/factory_2.png)

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/factory/factory.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/factory/factory.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/factory/factory.md)