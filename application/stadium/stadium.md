# Stadium

## Overview
<img src="docs/stadium_fm.png"  width="513" height="300">

[<img src="docs/stadium_3d_1.png"  width="152" height="100">](docs/stadium_3d_1.png)


### Descripton
The stadium is an important recreational infrastructure that constitutes a significant load for the grid, but only during specific events. The power consumption depends on the operating status of the facility and can be changed used the specific touch button: when no activity takes place, the consumption of the facility is almost zero; during a regular training the compunsation is increased by the need of providing lighting to the field; during a match the maximum consumption takes place.

### Interaction
<img src="docs/stadium_bd.png"  width="316" height= "400">

### Features
<img src="docs/stadium_ft_1.png"  width="300" height= "300">
<img src="docs/stadium_ft_2.png"  width="504" height= "300">
<img src="docs/stadium_ft_3.png"  width="421" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 164 x 148 x 65 [mm] (WxLxH)   |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 100 [mΩ]                      |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| LED Stand1| 12        |  60 mA  |  120 mA |
| LED Stand2| 12        |  60 mA  |  120 mA |
| LED Lights| 18        |  90 mA  |  180 mA |
| Speaker   | 1			|         |  125 mA	|
| SD-Reader | 1			|         |  500 mA	|
| INA 223	| 1			|		  |    5 mA |
| Total     |           |         | 1310 mA |

## Media

[<img src="docs/stadium_1.png" width="163" height= "100">](docs/stadium_1.png)
&nbsp;

## Related Links

* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/ec_station/ec_station.md)
* [Firmware](https://git.rwth-aachen.de/acs/public/teaching/legos/firmware/-/blob/master/components/ec_station/ec_station.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/ec_station/ec_station.md)