# Transmission Tower

## Overview
<img src="docs/transmission_tower_3d_1.png"  width="310" height="300">

### Descripton
The transmission tower is used to support an overhead power line in distributing the energy in the grid. The tower has no active purpose in the platform, but is rather a passive object that serves as a placeholder to cover those nodes that do not host any entity on top.

## Technical Details
|                   |                               |
| ----              | ----                          |
| Dimensions        | 100 x 100 x 180 [mm] (WxLxH)  |

## Media

[<img src="docs/transmission_tower_1.png" width="77" height= "100">](docs/transmission_tower_1.png)
&nbsp;
[<img src="docs/transmission_tower_2.png" width="76" height= "100">](docs/transmission_tower_2.png)

## Related Links

* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/transmission_tower/transmission_tower.md)