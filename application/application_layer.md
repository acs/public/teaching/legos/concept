# Application layer

<img src="docs/smart_building.png"  width="382" height="300">

## Description
The application layer consists of entities that represent down-scaled replicas of actual power grid actors. The power is generated and injected in the grid by the producers: while traditional power plants drive the voltage of the grid due to their high inertia, renewable plants rather inject current in the grid. Consumers absorb power from the grid and use it to provide a variety of services that range from lighting to storage charging, temperature regulation and others. Control loops run by default inside the entities, but they can be overridden by remote.

## Features
* quick assembling via custom designed spring-loaded connector
* energy-related behavioral patterns: consumer/producer, haptic-based, IoT-based
* communication based on MQTT message protocol with [UL2.0](https://fiware-iotagent-ul.readthedocs.io/en/latest/usermanual/) encoding
* independently IoT device profiles with power monitor capabilities

## Entities
### Infrastructure
* [Antenna](antenna/antenna.md): Base Transceiver Station, provides cellular network connectivity and edge-cloud services
* [Substation](substation/substation.md): essential element for the automation of an electrical grid, provides monitoring and protection functions; trips the reclosers in case of fault
* [Transmission Tower](transmission_tower/transmission_tower.md): distributes the power in the grid using overhead power lines
* [Action objects](entities/Objects.md): allows interaction with the grid inducing open/short circuit faults

### Producers
* [Power Plant](power_plant/power_plant.md): traditional fossil-fuel power plant, provides up to 60% of the total power and controls the voltage of the grid
* [Solar Farm](solar_farm/solar_farm.md): renewable energy power plant; injects current in the grid proportionally to the intensity of the incident light, optimized via 1-axis solar tracker
* [Wind Farm](wind_farm/wind_farm.md): renewable energy power plant; injects current in the grid proportionally to the wind intensity

### Consumers
* [Data Center](data_center/data_center.md): power critical infrastructure, essential in a cloud architecture; access to stored sensitive data triggers a data breach alert; uses a UPS in case of power sag/outage
* [EC Station](ec_station/ec_station.md): electric-car charging station; absorbs power from the grid according to custom charging profiles stored in the vehicle
* [Factory](factory/factory.md): common electrical load due to manufacturing activities; absorb power to operate industrial machinery like robotic arms
* [Hospital](hospital/hospital.md): power critical infrastructure, essential for healthcare services; the alert status is activated by the presence of service vehicles; uses a UPS in case of power sag/outage
* [Skyscraper](skyscraper/skyscraper.md): aggregated residential and commercial building; integrates a HVAC system for providing thermal comfort and acceptable indoor air quality; hosts a SCADA center for grid monitoring and for cyber-security
* [Stadium](stadium/stadium.md): power hungry entity occasionally hosts football matches; significant power absorbed in case of recreational events

### Prosumers
* [House](house/house.md): common residential electrical load, uses heating and air conditioning for indoor temperature regulation and integrates photovoltaic cells power generation from sunlight
* [Supermarket](supermarket/supermarket.md): common electrical load due to commercial activities, integrates a CHP system for combined electrical and thermal energy generation from gas
